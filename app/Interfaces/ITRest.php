<?php
    /**
     * Created by PhpStorm.
     * User: vikvikov
     * Date: 19/07/2019
     * Time: 15:42
     */
    namespace App\Interfaces;
    use Illuminate\Http\Request;
    interface ITRest
    {

        public function all();
        public function get($id);
        public function put(Request $request,$id);
        public function remove($id);

    }