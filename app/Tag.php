<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{

    protected $fillable = ["name", "slug", "category_id"];

    protected $dates = [];

    public static $rules = [
        "name" => "unique:Tags|required|max:255",
        "slug" => "unique:Tags|required|max:255",
        "category_id" => "numeric",
    ];

    public function getCategory()
    {
        return $this->hasOne("App\Category");
    }
}
