<?php namespace App\Http\Controllers;

use App\Traits\RESTActions;

class CategoriesController extends Controller
{

    const MODEL = "App\Category";

    use RESTActions;
}
