<?php namespace App\Http\Controllers;

use App\Interfaces\ITRest;
use App\Traits\RESTActions;

class TagsController extends Controller implements ITRest
{

    const MODEL = "App\Tag";

    use RESTActions;
}
