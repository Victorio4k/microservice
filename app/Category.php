<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{




    protected $fillable = ["name", "slug"];

    protected $dates = [];

    public static $rules = [
        "name" => "unique:categories|required|max:255",
        "slug" => "unique:categories|required|max:255",
    ];

    public function getTags()
    {
        return $this->hasMany("App\Tag");
    }
}
