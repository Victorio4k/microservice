<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

/**
 * Routes for resource category
 */
$router->get('category', 'CategoriesController@all');
$router->get('category/{id}', 'CategoriesController@get');
$router->post('category', 'CategoriesController@add');
$router->put('category/{id}', 'CategoriesController@put');
$router->delete('category/{id}', 'CategoriesController@remove');

/**
 * Routes for resource tag
 */
$router->get('tag', 'TagsController@all');
$router->get('tag/{id}', 'TagsController@get');
$router->post('tag', 'TagsController@add');
$router->put('tag/{id}', 'TagsController@put');
$router->delete('tag/{id}', 'TagsController@remove');
